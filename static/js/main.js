/*
	Main
*/

// Service worker
if ('serviceWorker' in navigator) {
	navigator.serviceWorker.register('./sw.js')
		.then((reg) => console.log('Service worker registered', reg))
		.catch((err) => console.log('Servicer worker not registered', err))
}

// Components
import * as Chart from './components/chart.js'
import * as Loader from './components/loader.js'
import * as Navbar from './components/navbar.js'
import * as Search from './components/search.js'

// App
var app = new Vue({
	el: 'main',
	components: {
		'chart': Chart.component,
		'loader': Loader.component,
		'navbar': Navbar.component,
		'search': Search.component
	}
})

// Hash change listener
window.addEventListener('hashchange', function() {
	let ticker = window.location.hash.replace('#ticker=', '');
	// Ticker set
	if (ticker != '') {
		// Call API
		apiCall(ticker);
		
		// Display loader
		setTimeout(() => {
			app.$refs.loader.active = true;
		}, 600); // Search out animation time
	}
	// No ticker
	else {
		app.$refs.navbar.back = false;
		setTimeout(() => {
			app.$refs.chart.out = true;
			// Reset search
			setTimeout(() => {
				app.$refs.search.query = null;
				app.$refs.search.results = [];
				app.$refs.search.ticker = null;
				app.$refs.search.out = false;
			}, 600); // Chart out time
		}, 150); // Delay from back button animation
	}
}, false);

// Api call
const apiKey = 'y4_spmKQBFEue-TK-Kdf';
const apiUrl = 'https://www.quandl.com/api/v3/datasets/WIKI/{ticker}.json?api_key='+apiKey+'&column_index=4&collapse=monthly';

function apiCall(ticker) {
	let xhttp = new XMLHttpRequest();
		xhttp.addEventListener("load", apiCallback);
		xhttp.open("GET", apiUrl.replace('{ticker}', ticker));
		xhttp.send();
}

function apiCallback () {
	setTimeout(() => {
		app.$refs.loader.active = false;
	}, 1000); // Some faked loading time

	// Success
	if (this.status === 200) {
		let response = JSON.parse(this.responseText);
		console.log(response);
		app.$refs.chart.name = response.dataset.name.split(' (')[0] + ' Price in $.'; // The name from the API is not in a satisfactory format
		app.$refs.chart.ticker = response.dataset.dataset_code;
		app.$refs.chart.timeseries = response.dataset.data;
	}
	// Fail
	else {
		alert('Something went wrong when trying to connect to the API.');
	}

	setTimeout(() => {
		app.$refs.chart.out = false;
		app.$refs.chart.active = true;
		setTimeout(() => {
			app.$refs.navbar.back = true;
		}, 600);
	}, 2000); // Wait for faked loading time and loader animation
}