let template = '<div class="navbar" v-bind:class="{ active: active }">'+
					'<button class="back" v-bind:class="{ active: back }" v-on:click="backClick($event)">'+
						'<span class="material-icons">chevron_left</span>'+
					'</button>'+
					'<div class="logo"></div>'+
					'<div class="text"></div>'+
				'</div>';

let component = {
	data: function() {
		return {
			active: false,
			back: false,
			out: false
		};
	},
	methods: {
		backClick: function(event) {
			window.location.hash = "";	
		}
	},
	mounted: function() {
		setTimeout(() => {
			this.active = true
		}, 6150); // Animation timing for faked loader
	},
	template: template
}

export { component }