import * as Tickers from '../data/tickers.js'

let template = '<div class="search" v-bind:class="{ active: active, out: out }">'+
					'<div class="wrapper-narrow">'+
						'<label>Stock Ticker Symbol</label>'+
						'<div class="field" v-bind:class="{ blur: blur }">'+
							'<input v-model="query" type="text" spellcheck="false" placeholder="E.g. AMZN" v-on:blur="blur = true" v-on:focus="blur = false" @keyup="keyup($event)" />'+
							'<span class="material-icons">search</span>'+
						'</div>'+
					'</div>'+
					'<div class="wrapper-narrow">'+
						'<ul class="search-results" v-if="results.length > 0">'+
							'<li v-for="result of results" v-bind:class="{ active: ticker == result }" v-on:click="resultClick(result)">{{ result }}<span class="material-icons">chevron_right</span></li>'+
						'</ul>'+
					'</div>'+
				'</div>';

let component = {
	data: function() {
		return {
			active: false,
			blur: true,
			out: false,
			query: null,
			results: [],
			ticker: null
		};
	},
	methods: {
		keyup(event) {
			let count = 0;
			let result = [];

			// Set empty results if there is no query
			if (this.query.length < 1) {
				this.results = [];
			}
			// Find results
			else {
				for (let object of Tickers.data) {
					if (count < 6) {
						if (typeof object.ticker == "string") {
							if (object.ticker.startsWith(this.query.toUpperCase())) {
								result.push(object.ticker);
								count++
								if (count == 5) {
									break;
								}
							}
						}
					}
				}
				this.results = result;
			}
		},
		resultClick(result) {
			this.ticker = result;
			window.location.hash = "ticker=" + this.ticker;
			this.out = true;
		}
	},
	mounted: function() {
		setTimeout(() => {
			this.active = true
		}, 6450); // Animation timing for faked loader
	},
	template: template
}

export { component }