let template = '<div class="chart" v-bind:class="{ active: active, out: out }">'+
					'<div class="wrapper">'+
						'<label>Chart for Ticker {{ ticker }}</label>'+
						'<h2>{{ name }}</h2>'+
						'<div id="chart-container"></div>'+
					'</div>'+
				'</div>';

let component = {
	data: function() {
		return {
			active: false,
			name: '',
			out: false,
			ticker: '',
			timeseries: []
		};
	},
	template: template,
	watch: {
		timeseries: function (values) {
			// Parse values in reverse order (.reverse() exposes Vue weekness with watch:)
			let valuesParsed = [];
			for (var i = values.length-1; i >= 0; i--) {
				valuesParsed.push(values[i]);
			}

			// Render chart
			Highcharts.chart('chart-container', {
				chart: {
					backgroundColor: 'transparent',
				},
				title: {
					text: ''
				},
				yAxis: {
					labels: {
						style: {
							fontSize: '15px',
							fontFamily: '"Roboto", sans-serif',
							color: 'rgba(255,255,255,0.35)'
						},
					},
					title: null,
					gridLineColor: 'rgba(255,255,255,0.1)'
				},
				xAxis: {
					accessibility: {
						rangeDescription: 'Range: '+valuesParsed[0][0]+' to '+valuesParsed[values.length-1][0]
					},
					visible: false
				},
				legend: {
					enabled: false
				},
				series: [{
					name: '',
					data: valuesParsed,
					color: 'rgba(252,225,182,1)'
				}],
				responsive: {
					rules: [{
						condition: {
							maxWidth: 800
						},
						chartOptions: {
							legend: {
								layout: 'horizontal',
								align: 'center',
								verticalAlign: 'bottom'
							}
						}
					}]
				}
			});
		}
	}
}

export { component }