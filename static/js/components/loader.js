let template = '<div class="loader" v-bind:class="{ active: active }">'+
					'<div class="img"></div>'+
				'</div>';

let component = {
	data: function() {
		return {
			active: false
		};
	},
	mounted: function() {
		setTimeout(() => {
			this.active = true
		}, 10); // Make sure the DOM is loaded and trigger a change
		setTimeout(() => {
			this.active = false
		}, 5000); // Animation timing for faked loader
	},
	template: template
}

export { component }