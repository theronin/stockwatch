/*
	Service Worker
*/

// Const
const cacheStatic = 'static';
const requests = [
	'/',
	'https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap',
	'https://fonts.googleapis.com/icon?family=Material+Icons',
	'/static/css/components/chart.css',
	'/static/css/components/loader.css',
	'/static/css/components/navbar.css',
	'/static/css/components/search.css',
	'/static/css/general/animations.css',
	'/static/css/general/forms.css',
	'/static/css/general/reset.css',
	'/static/css/general/template.css',
	'/static/css/general/typography.css',
	'/static/css/main.css',
	'/static/img/bg.jpg',
	'/static/img/logo_symbol.png',
	'/static/img/logo_text.png',
	'/static/js/components/chart.js',
	'/static/js/components/loader.js',
	'/static/js/components/navbar.js',
	'/static/js/components/search.js',
	'/static/js/data/tickers.js',
	'/static/js/vendor/vue.min.js',
	'/static/js/main.js',
	'/static/json/manifest.json'
];

// Event Listeners
self.addEventListener('install', event => {
	event.waitUntil(
		caches.open(cacheStatic).then(cache => {
			cache.addAll(requests);
		})
	);
});

self.addEventListener('fetch', event => {
	event.respondWith(
		caches.match(event.request).then(cacheResponse => {
			return cacheResponse || fetch(event.request)
		})
	);
});