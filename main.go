package main

import (
	"mime"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.New()
	router.Use(gin.Logger())
	router.LoadHTMLGlob("tmpl/*.tmpl")
	router.Static("/static", "./static")
	router.StaticFile("/sw.js", "./sw.js")
	mime.AddExtensionType(".js", "application/javascript")
	router.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.tmpl", nil)
	})
	router.Run(":80")
}
